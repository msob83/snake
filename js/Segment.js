export class Segment{
    constructor(x,y,a,drawer,color){
        this.x = x;
        this.y = y;
        this.a = a;
        this.drawer = drawer;
        this.color = color;
    }
    drawMyself(){
        this.drawer.drawSquare(this.x,this.y,this.a,this.color);
    }


}