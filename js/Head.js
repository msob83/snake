export class Head{
    constructor(segment,img){
        this.segment = segment;
        this.img = img;
    }
    drawMyself(){
        this.segment.drawMyself();
        this.segment.drawer.drawXmasCap(this.segment.x,this.segment.y,this.segment.a,this.img);
    }
}