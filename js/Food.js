export class Food{
    constructor(x,y,a,drawer,image){
        this.x = x;
        this.y = y;
        this.a = a;
        this.image = image;
        this.drawer = drawer;
    }
    drawMyself(){
        this.drawer.drawLollipop(this.x,this.y,this.a,this.image);
    }
}