import {Head} from "./Head.js";
import {Segment} from "./Segment.js";
import {canvas_dimens, colors, img, unit} from "./constants.js";

export class Snake{
    constructor(segments,interval){
        this.segments = [...segments];
        this.grow = true;
        this.interval = interval;
    }
    move(vector){
        this.segments.unshift(new Head( new Segment(this.segments[0].segment.x+vector[0],this.segments[0].segment.y+vector[1],unit,this.segments[0].segment.drawer,colors.last_segment) , img));
        if(this.segments.length > 1){
            // the second segment is not a head
            this.segments[1] = new Segment(this.segments[1].segment.x,this.segments[1].segment.y,unit,this.segments[1].segment.drawer,colors.segment);

            if(!this.grow){
                this.segments.pop();

            }else{
                this.grow = false;
            }
        }
    }
    drawMyself(){
        for(let i = 1; i < this.segments.length; i++){
            this.segments[i].drawMyself();
        }
        this.segments[0].drawMyself();
    }

    isPlaceTaken(x,y){

        if(this.segments[0].segment.x === x && this.segments[0].segment.y === y ){

            return true;
        }

        if(this.segments.length < 2){
            return false;
        }

        return  this.segments.slice(1).some(function(s){
            return s.x === x && s.y === y;
        });
    }
    foodControl(foods){
        let segments = this.segments; // cant use this this in function findIndex
        let food_index= foods.findIndex(function(f){
            return f.x === segments[0].segment.x && f.y === segments[0].segment.y;
        });

        if(food_index > -1){
            this.grow = true;
            foods.splice(food_index,1);
        }
    }
    detectWallCollision(){
        if(this.segments[0].segment.x > canvas_dimens || this.segments[0].segment.x < 0 || this.segments[0].segment.y < 0 || this.segments[0].segment.y > canvas_dimens ) {
            alert("LOOSER");
            clearInterval(this.interval);
            window.location.href= window.location.href;
        }
    }

    detectSelfCollision(){
        let segments = this.segments;
        let head = this.segments[0];
        if(segments.slice(1).some(function(s){
            return s.x === head.segment.x  && s.y === head.segment.y
        })){
            alert("LOOSER  - SELF COLLISION");
            clearInterval(this.interval);
            window.location.href= window.location.href;
        }
    }
}