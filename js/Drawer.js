import {ctx, unit} from "./constants.js";

export class Drawer{

    drawSquare(x,y,a,color){
        ctx.fillStyle = color;
        ctx.fillRect(x,y,a,a);
    }
    drawXmasCap(x,y,a,img){
        ctx.drawImage(img,x,y-unit/2,a,a);
    }
    drawLollipop(x,y,a,img){
        ctx.drawImage(img,x,y,a,a);
    }

}