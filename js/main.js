import {ctx,img,lollipop_img,canvas_dimens,unit,colors} from "./constants.js";
import {Drawer} from "./Drawer.js";
import {Segment} from "./Segment.js";
import {Head} from "./Head.js";
import {Food} from "./Food.js";
import {Snake} from "./Snake.js"


let interval = setInterval(function(){
    ctx.clearRect(0,0,canvas_dimens,canvas_dimens);
    snake.drawMyself();
    if(foods.length == 0){
        generateFood();
    }else{
        foods[0].drawMyself();
    }
    snake.foodControl(foods);

    snake.detectWallCollision();
    snake.detectSelfCollision();
    snake.move(vector);

},200);



let drawer = new Drawer();
let segments = [new Head( new Segment(0,0,unit,drawer,colors.last_segment) , img)];

let snake = new Snake(segments,interval);
let vector = [0,unit];

let foods = [];



document.onkeydown = function (e) {

    switch (e.key) {
        case 'ArrowLeft':
            if(snake.segments.length > 1 && Math.abs(vector[0]) > 0){
                return false;
            }
            vector = [-unit,0];
            //alert('left');
            break;
        case 'ArrowUp':
            if(snake.segments.length > 1 && Math.abs(vector[1]) > 0){
                return false;
            }
            vector = [0,-unit];
            //alert('up');
            break;
        case 'ArrowRight':
            if(snake.segments.length > 1 && Math.abs(vector[0]) > 0){
                return false;
            }
            vector = [unit,0];
            //alert('right');
            break;
        case 'ArrowDown':
            if(snake.segments.length > 1 && Math.abs(vector[1])> 0){
                return false;
            }
            vector = [0,unit];
            //alert('down');
            break;
    }
}


function generateFood(){

    let freePlaces = [];
    let units = canvas_dimens;

    for(let x  = 0; x < canvas_dimens; x=x+unit  ){
        for( let y = 0; y < canvas_dimens; y=y+unit){

            if(!snake.isPlaceTaken(x,y)){
                freePlaces.push([x,y]);
            }
        }
    }
    if(freePlaces.length === 0){
        return false;
    }
    let place = freePlaces[Math.floor(Math.random()*freePlaces.length)];
    foods.push(new Food(place[0],place[1],unit,drawer,lollipop_img));


}














