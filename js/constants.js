export const canvas = document.getElementById("canvas");
export const ctx = canvas.getContext("2d");
export const img = document.getElementById("xmasCap");
export const lollipop_img = document.getElementById("lollipop");

export const canvas_dimens = 700;

export const unit = 25;
export const colors = {
    'segment':'#00ff00',
    'last_segment': '#004d1a'
};
